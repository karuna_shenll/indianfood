<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
// include_once("header.php");
$Page = 1;$RecordsPerPage = 25;
$TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["orderSearchCriteria"])) {
    $orderSearch = json_decode($_POST["orderSearchCriteria"], true);
    $vendorId = !empty($orderSearch["vendor_id"]) ? $orderSearch["vendor_id"] : "" ;    
    $orderType = !empty($orderSearch["order_type"]) ? $orderSearch["order_type"] : "" ;
    if (isset($orderSearch['HdnPage']) && is_numeric($orderSearch['HdnPage']))
        $Page = $orderSearch['HdnPage'];
}
?>
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />

<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">                
<div class="portlet-body" style="padding-top: 0px;">
    <table class="table table-striped table-bordered table-hover" id="food-orders-list">
        <thead>
            <tr>
                <th>Order ID</th>
                <th>Customer name</th>
                <th>Email</th>                                
                <!-- <th>Mobile number</th> -->
                <th>Food Type</th>
                <th>Price</th>
                <th>Ratings</th>
                <th>Status</th>
                <th>Start date</th>
                <th>End date</th>
            </tr>
        </thead>
        <tbody>
        		<?php
                // echo $Page;exit;
                $qryParams = array();
                $orderTypeCodtn = $vendorCondtn = "";
                $whereCondtn = "";
                // if ($vendorId) {                    
                    $vendorCondtn .= (!empty($vendorId))?" vendor_id = :vendor_id":"";
                    $orderTypeCodtn .= (!empty($orderType))?" and order_type = :food_type":"";                    
                    if (!empty($vendorId)) {
                        $whereCondtn = " WHERE vendor_id = :vendor_id";
                    } elseif(!empty($vendorId)) {
                        $whereCondtn = " WHERE order_type = :food_type";
                    } elseif (!empty($vendorId) && !empty($orderTypeCodtn)) {
                        $whereCondtn = " WHERE vendor_id = :vendor_id AND order_type = :food_type";
                    }
                    $vendorderQry = "SELECT *,ordr.status as order_status FROM `tbl_orders` as ordr join tbl_users as usr on ordr.customer_id = usr.user_id $whereCondtn order by ordr.order_id asc";
                    // $qryParams[':vendor_id'] = $vendorId;
                    if(!empty($vendorId))
                        $qryParams[':vendor_id'] = $vendorId;
                    if(!empty($orderType))
                        $qryParams[':food_type'] = $orderType;
                    // echo count($qryParams);exit;
                    $getVendorders = $foodAppApi->funBckendExeSelectQuery($vendorderQry,$qryParams);
        			$vendorderlist="";
        			if(count($getVendorders,COUNT_RECURSIVE)>1) {
                        $TotalPages=ceil(count($getVendorders)/$RecordsPerPage);
                        $Start=($Page-1)*$RecordsPerPage;
                        $sno=$Start+1;
                        $vendorderQry.=" limit $Start,$RecordsPerPage";
                        $getVend_orders = $foodAppApi->funBckendExeSelectQuery($vendorderQry,$qryParams);
                        $i=1;
        				foreach($getVend_orders as $fetchVendorders) {
        					$full_name 		=  $fetchVendorders['full_name']; // customer name
        					$email 			=  $fetchVendorders['email'];
        					$mobile_number 	=  $fetchVendorders['mobile_number'];
                            $start_date     =  $fetchVendorders['start_date'];
                            $end_date       =  $fetchVendorders['end_date'];
                            $price          =  $fetchVendorders['price'];
                            $ratings        =  $fetchVendorders['ratings'];
                            $order_type     =  $fetchVendorders['order_type'];
                            $order_status   =  $fetchVendorders['order_status']; // order status
                            $foodtypeimg    =   "";                                            
                            $foodtypeimg    =  ($order_type=='veg')?"<img src='../assets/layouts/layout2/img/veg.png'>":"<img src='../assets/layouts/layout2/img/non-veg.png'>";
        					// Lising the vendors
        					$vendorderlist.="<tr>";
        					// $vendorderlist.="<td>".$i++."</td>";
                            $vendorderlist.="<td>".$fetchVendorders['order_id']."</td>";
        					$vendorderlist.="<td>".$full_name."</td>";
        					$vendorderlist.="<td>".$email."</td>";
        					// $vendorderlist.="<td>".$mobile_number."</td>";
                            $vendorderlist.="<td>".$foodtypeimg."  ".$order_type."</td>";
        					$vendorderlist.="<td>".$price."</td>";
                            $vendorderlist.="<td><div id='order_$i'></div></td>";
                            $vendorderlist.="<td>".$order_status."</td>";
                            $vendorderlist.="<td nowrap>".$start_date."</td>";
                            $vendorderlist.="<td nowrap>".$end_date."</td>";
                            $rating_id = "#order_$i";
                            $vendorderlist.="<script>
                               $('".$rating_id."').rateYo({rating: $ratings,readOnly: true,starWidth: '15px'});
                            </script>";
                            $i++;
        				}
        				echo $vendorderlist;
        			}
                // }
        		?>          		
        </tbody>
    </table>
</div>
<?php
if ($TotalPages > 1) {
    echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
    $FormName = "vendorderslisting_form";
    require_once ("paging.php");
    echo "</td></tr>";
}
?>
<script src="../assets/layouts/layout2/scripts/filter_vendor_orders_listing.js" type="text/javascript"></script>