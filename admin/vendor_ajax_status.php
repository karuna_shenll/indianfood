<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);
if(isset($_POST["status"]) && isset($_POST["vendorid"])) {
	$status=$_POST["status"];
	$vendorid=$_POST["vendorid"];
    $Qry="update tbl_users set status=:status where user_id=:user_id";
    $qryParams[':status'] = $status;
    $qryParams[':user_id'] = $vendorid;
    $getcustomer = $foodAppApi->funBckendExeUpdateRecord($Qry,$qryParams);
    if($getcustomer){
      echo "success";
      exit;
    } else {
      echo "failure";
      exit;
    }
} else {
    echo "failure";
    exit;
}