<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');

$Page = 1;$RecordsPerPage = 25;
$TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["orderSearchCriteria"])) {
    // echo "Test";
    $orderSearch = json_decode($_POST["orderSearchCriteria"], true);
    if (isset($orderSearch['HdnPage']) && is_numeric($orderSearch['HdnPage']))
        $Page = $orderSearch['HdnPage'];
   
}
?>
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnRecordsPerPage" id="HdnRecordsPerPage" value="<?php echo $RecordsPerPage; ?>">

<div class="portlet-body ">
    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_invoice_list">
        <thead class="flip-content">
            <tr>
                <th nowrap>Invoice #</th>
                <th>Aunty</th>
                <th>Transaction Id</th>
                <th>Amount</th>
                <th>Aunty Status</th>
                <!-- <th>Admin Status</th> -->
            </tr>
        </thead>
        <tbody>
            <?php
                $ArrStatus = array('pending','paid');
                $statuslist = implode("','",$ArrStatus);
                $Qry="SELECT user.full_name,invoice.invoice_id,invoice.vendor_id,invoice.transaction_id,invoice.total_amount,invoice.invoice_amount,invoice.discount,invoice.vendor_invoice_status,invoice.admin_invoice_status FROM tbl_invoices as invoice INNER JOIN tbl_users as user ON user.user_id=invoice.vendor_id where user.status=:status and user_type=:user_type and upload_documents<>'' and invoice.admin_invoice_status!='' ORDER BY FIELD(invoice.vendor_invoice_status,'".$statuslist."')"; 

                $qryParams[":user_type"]="vendor";
                $qryParams[":status"]="Active";
                $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                
                if (count($getResCnt,COUNT_RECURSIVE)>1) {
                    $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                    $Start=($Page-1)*$RecordsPerPage;
                    $sno=$Start+1;
                    $Qry.=" limit $Start,$RecordsPerPage";
                    $getInvoices = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                    if (count($getInvoices)>0) {
                        foreach ($getInvoices as $invoicesListData) {
                           $statusVenColor=(strtolower($invoicesListData["vendor_invoice_status"])=="paid")?"success":"info";
                           $statusAdmColor=(strtolower($invoicesListData["admin_invoice_status"])=="paid")?"success":"info";
                            ?>
                           <tr>
                                <td><?php echo $invoicesListData["invoice_id"];?></td>
                                <td><?php echo $invoicesListData["full_name"];?></td>
                                <td><?php echo $invoicesListData["transaction_id"];?></td>
                                <td><?php echo "$".$invoicesListData["invoice_amount"];?></td>
                                <td>
                                    <span class="label label-sm label-<?php echo $statusVenColor?>"> <?php echo $invoicesListData["vendor_invoice_status"];?></span>
                                </td>
                                <!-- <?php if (strtolower($invoicesListData["vendor_invoice_status"])=="paid" && strtolower($invoicesListData["admin_invoice_status"])=="pending") { ?>
                                    <td><span class="label label-sm label-<?php echo $statusAdmColor?>"><button type="button" style="text-decoration: underline;" class="invoice_status_btn" data-invoice-id="<?php echo $invoicesListData["invoice_id"];?>" id="updateinvoicestatus" ><?php echo $invoicesListData["admin_invoice_status"];?></span></button></td>
                                <?php } else { ?>
                                <td> <span class="label label-sm label-<?php echo $statusAdmColor?>"><?php echo $invoicesListData["admin_invoice_status"];?></span></td>
                                <?php } ?> -->
                                
                           </tr>
            <?php   
                $sno++;     
                    }
                }
                // else {
                //     echo "<tr><td colspan='6' style='text-align:center;'>No invoice(s) found </td></tr>";
                // }
            } 
            // else {
            //     echo "<tr><td colspan='6' style='text-align:center;'>No invoice(s) found </td></tr>";
            // }

            ?>
            
        </tbody>
    </table>

    
</div>
<?php
    if ($TotalPages > 1) {

        echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
        $FormName = "invoice_list_form";
        require_once ("paging.php");
        echo "</td></tr>";
    }
?>
<script>

var table = $('#tbl_invoice_list');
var oTable = table.dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No invoice(s) found",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "No invoice(s) found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found"
    },

    "searching": false,
    "bPaginate": false,
    "bInfo" : false,

    // Or you can use remote translation file
    //"language": {
    //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
    //},


    buttons: [
        // { extend: 'print', className: 'btn dark btn-outline' },
        // { extend: 'copy', className: 'btn red btn-outline' },
        // { extend: 'pdf', className: 'btn green btn-outline' },
        // { extend: 'excel', className: 'btn yellow btn-outline ' },
        // { extend: 'csv', className: 'btn purple btn-outline ' },
        // { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
    ],

    // setup responsive extension: http://datatables.net/extensions/responsive/
    responsive: true,

    // Disable sorting - particular columns
    "columnDefs": [ {
        "targets": [2, 3, 4], // column or columns numbers
        "orderable": false,  // set orderable for selected columns
    }],

    //"ordering": false, disable column ordering 
    //"paging": false, disable pagination
    
    "order": [
        [0, 'asc']
    ],
    
    "lengthMenu": [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"] // change per page values here
    ],
    // set the initial value
    "pageLength": 10,

    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable invoice-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
    // So when dropdowns used the scrollable div should be removed. 
    //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
});
</script>