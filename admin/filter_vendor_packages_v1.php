<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
// include_once("header.php");
// $Page = 1;$RecordsPerPage = 25;
// $TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["orderSearchCriteria"])) {
    $orderSearch = json_decode($_POST["orderSearchCriteria"], true);
    $vendor 	 = !empty($orderSearch["vendor_id"]) ? $orderSearch["vendor_id"] : "" ;    
    $packageName = !empty($orderSearch["package_name"]) ? $orderSearch["package_name"] : "" ;
    $packageType = !empty($orderSearch["package_type"]) ? $orderSearch["package_type"] : "" ;
    // if (isset($orderSearch['HdnPage']) && is_numeric($orderSearch['HdnPage']))
    //     $Page = $orderSearch['HdnPage'];
}
?>
<style>
	.txt_center {
		text-align: center;
	}
	.page-container-bg-solid .page-content {
        background: #fefeff !important;
    }
    #vendor-package-content {
        min-height: 510px !important;
    }
</style>
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="food-packages-list">
        <thead>
            <tr>
                <th>#</th>
                <th>Package</th>
                <th>Number Of Days</th>
                <th>Image</th>
                <th>Type</th>
                <th>Rate</th>
            </tr>
        </thead>
        <tbody>
    		<?php
    		$Arrpackage =array();$foodtypcondn="";
            if ($vendor) {
                $foodtypcondn.= (!empty($packageType))?" and package_type = :package_type":"";
                $foodtypcondn.= (!empty($packageName))?" and pck.package_id = :package_name":"";
                $vendpackageQry = "SELECT * FROM `tbl_packages` as pck JOIN tbl_package_items as pckitm ON pck.package_id = pckitm.package_id JOIN tbl_category_items as catitm ON catitm.item_id = pckitm.item_id where vendor_id = :vendor_id $foodtypcondn";
                $qryParams[':vendor_id'] = $vendor;
                if(!empty($packageType))
                    $qryParams[':package_type'] = $packageType;
                if(!empty($packageName))
                    $qryParams[':package_name'] = $packageName;
                
                $getVendorpackage = $foodAppApi->funBckendExeSelectQuery($vendpackageQry,$qryParams);
    			$i=1;$vendorderlist="";
    			if(count($getVendorpackage,COUNT_RECURSIVE)>1) {
    				foreach($getVendorpackage as $fetchVendorpackage) {
    					$package_name 			=  	$fetchVendorpackage['package_name'];
    					$package_days_count 	= 	$fetchVendorpackage['package_days_count'];
    					$package_type 			=  	$fetchVendorpackage['package_type'];
                        $rate     				=  	$fetchVendorpackage['rate'];
                        $item_name       		=  	$fetchVendorpackage['item_name'];
                        $package_id 			= 	$fetchVendorpackage['package_id'];
                        $item_type 				= 	$fetchVendorpackage['item_type'];
                        $price 					= 	$fetchVendorpackage['price'];
                        $Arrpackage[$package_id]= 	$package_id;
                        $Arrpackagename[$package_id][$package_name] =  $package_name;
                        $Arrpackagedetails[$package_id][$package_name]['count'] = $package_days_count;
                        $Arrpackagedetails[$package_id][$package_name]['type'] = $package_type;
                        $Arrpackagedetails[$package_id][$package_name]['rate'] = $rate;
                        $Arrpackageitems[$package_id][$package_name][$item_name]['item_type'] = $item_type;
                        $Arrpackageitems[$package_id][$package_name][$item_name]['price'] = $price;
    				}
    			}
            }
            if (count($Arrpackage)>0){
            	$vendpackagelist="";$i=1;
        		foreach($Arrpackage as $package_id) {
        			foreach($Arrpackagename[$package_id] as $package_name) {
        				$vendpackagelist.="<tr>";
        				$vendpackagelist.="<td>".$i++."</td>";
        				$vendpackagelist.="<td><a data-href=pack_".$package_id." class='pack'>".$package_name."</a></td>";
        				$vendpackagelist.="<td>" .$Arrpackagedetails[$package_id][$package_name]['count']."</td>";
        				$vendpackagelist.="<td colspan='1'>image</td>";
        				$vendpackagelist.="<td>" .$Arrpackagedetails[$package_id][$package_name]['type']."</td>";
        				$vendpackagelist.="<td>" .$Arrpackagedetails[$package_id][$package_name]['rate']."</td>";
        				$vendpackagelist.="</tr>";
        				// $vendpackagelist.="<tr><td colspan='6'><div>";	
        				// echo $package_name."==>";
        				$vendpackagelist.="<table class='table table-striped table-bordered table-hover hiddenRow' id=pack_".$package_id.">
        					<thead>
                    			<tr>
	                                <th>Item</th>
	                                <th>Type</th>
	                                <th>Image</th>
	                                <th>Price</th>
	                            </tr>
	                            </thead><tbody>";
        				foreach($Arrpackageitems[$package_id][$package_name] as $items=>$Arritems) {
        					$vendpackagelist.="<tr>
				                                <td>".$items."</td>
				                                <td>".$Arritems['item_type']."</td>
				                                <td>item image</td>
				                                <td>".$Arritems['price']."</td>
				                            </tr>";								            
        				}
        				$vendpackagelist.="</tbody></table></div></td></tr>";
        			}
        		}
        		echo $vendpackagelist;
            }  else {
            	echo "<tr><td colspan='6' class='txt_center'>No Package(s) found</td></tr>";
            }
    		?>	                        		
        </tbody>
    </table>
</div>

<script>
	$(document).ready(function(){
	 	$(".pack").on("click",function(){        	
	    	var refid = $(this).attr("data-href");
	    	if ($("#"+refid).is(':visible')) {
	    		$("#"+refid).slideUp();
	    		return false;
	    	}
	    	$(".hiddenRow").slideUp();
	    	$("#"+refid).slideDown();
	    });
    });
</script>