<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);
if(isset($_REQUEST["status"]) && isset($_REQUEST["customerid"])) {
	$status=$_REQUEST["status"];
	$customerid=$_REQUEST["customerid"];
    $Qry="update tbl_users set status=:status where user_id=:customerid";
    $qryParams[':status'] = $status;
    $qryParams[':customerid'] = $customerid;
    $getcustomer = $foodAppApi->funBckendExeUpdateRecord($Qry,$qryParams);
    if($getcustomer){
      echo "success";
      exit;
    } else {
      echo "failure";
      exit;
    }
} else {
    echo "failure";
    exit;
}