<?php
include_once('../includes/configure.php');
//session_start();
include_once('../api/Common.php');
//include("header.php");
$foodAppApi = new Common($dbconn);
if(isset($_REQUEST["HdnMode"])){
	$RecordsPerPage=$_REQUEST["PerPage"];
	$Page=1;
	$TotalPages=0;
    $fromdate  =  $_REQUEST['fromdate'];
    $todate    =  $_REQUEST['todate'];
    $vendorsid =  $_REQUEST['vendorsid'];
    $customerid=  $_REQUEST['customerid'];  
    $_SESSION["reports"]["HdnPage"] = $Page;
}

//print_r($_REQUEST);
$_SESSION["reports"]["hndfromdate"] = $fromdate;
$_SESSION["reports"]["hdntodate"] = $todate;
$_SESSION["reports"]["hdnvendorsid"] = $vendorsid;
$_SESSION["reports"]["hdncustomerid"] = $customerid;

$Qrycondition="";   
$qryParams=array();
if(!empty($customerid)) {
 	$Qrycondition.=" and orders.customer_id=:customerid";
 	$qryParams[":customerid"]=$customerid;
}
if(!empty($vendorsid)) {
 	$Qrycondition.=" and orders.vendor_id=:vendorid";
 	$qryParams[":vendorid"]=$vendorsid;
}
if(!empty($fromdate)) {
 	$Qrycondition.=" and DATE_FORMAT(orders.start_date, '%Y-%m-%d')>=:startdate"; 
 	$qryParams[":startdate"]=date("Y-m-d",strtotime($fromdate));
}
if(!empty($todate)) {
 	$Qrycondition.=" and DATE_FORMAT(orders.end_date, '%Y-%m-%d')<=:enddate";
 	$qryParams[":enddate"]=date("Y-m-d",strtotime($todate));
}

$Qry="SELECT users.full_name,orders.order_id,orders.vendor_id,orders.start_date,orders.end_date,orders.price,orders.ratings,orders.order_type,orders.status FROM tbl_orders as orders INNER JOIN tbl_users as users ON users.user_id=orders.vendor_id where orders.order_id !='' ".$Qrycondition." order by orders.order_id desc";

?>
<form name="reportlist_form" id="reportlist_form" method="post" action="">
	<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
	<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
	<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
	<input type="hidden" name="hndfromdate" id="hndfromdate" value="<?php echo $fromdate ?>">
	<input type="hidden" name="hdntodate" id="hdntodate" value="<?php echo $todate?>">
	<input type="hidden" name="hdnvendorsid" id="hdnvendorsid" value="<?php echo $vendorsid ?>">
	<input type="hidden" name="hdncustomerid" id="hdncustomerid" value="<?php echo $customerid;?>">  
    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_reports_list">
    <div class="portlet-body flip-scroll" id="sample">
    	
            <thead class="flip-content">
                <tr>
                	<th width="5%">Id</th>
                    <th nowrap>Aunty</th>
                    <th>Order #</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Rating</th>
                    <th>Price</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            	<?php
                    $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                    // echo count($getResCnt);
                    if (count($getResCnt,COUNT_RECURSIVE)>1) {
                        $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                        $Start=($Page-1)*$RecordsPerPage;
                        $sno=$Start+1;
                        $Qry.=" limit $Start,$RecordsPerPage";
                   		$getReports = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
               		    //$sno=1;
				   	    if (count($getReports)>0) {
				   		    foreach ($getReports as $reportsListData) {
				   			   $statusColor=(strtolower($reportsListData["status"])=="pending")?"red":"green";

				?>
				       <tr>
				       		<td><?php echo $sno;?></td>
				       		<td><?php echo $reportsListData["full_name"];?></td>
				       		<td><?php echo $reportsListData["order_id"];?></td>
				       		<td><?php echo date("d/m/Y",strtotime($reportsListData["start_date"]));?></td>
				       		<td><?php echo date("d/m/Y",strtotime($reportsListData["end_date"]));?></td>
				       		<td><?php echo $reportsListData["ratings"];?></td>
						    <td><?php echo $reportsListData["price"];?></td>
				       		<td style="color:<?php echo $statusColor?>"><?php echo $reportsListData["status"];?></td>
				       </tr>
                <?php   
				    $sno++;		
			            }
					} else {
                        echo "<tr><td colspan='9' style='text-align:center;'>No report(s) found </td></tr>";
					}
                } else {
                    echo "<tr><td colspan='9' style='text-align:center;'>No report(s) found </td></tr>";
                }

                ?>
                
            </tbody>
        </table>
    </div>
    <div>
        <?php
            if($TotalPages > 1){

                echo "<table style='text-align:center;width:478px;margin:auto;'><tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
                $FormName = "reportlist_form";
                require_once ("paging.php");
                echo "</td></tr></table>";
            }
        ?>
    </div>
</form>