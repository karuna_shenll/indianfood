<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);

$invoiceHTMLStart="";$invoiceHTML="";

if(isset($_REQUEST["orderId"]) && $_REQUEST["orderId"]!="" || $_REQUEST["orderId"]!="0") {
	$packageId = $_REQUEST["packageId"];
	$orderId = $_REQUEST["orderId"];
	$packageName = "";

    $invoiceHTMLStart='<style>
		    #order_tbl>tr>td {font-size:5px;}
		    #order_tbl>tr>td, #order_tbl>th>td {border:0.4px solid #ccc;}
	    </style>
	    <table width="100%" bgcolor="#F7F7F7" id="order_tbl" cellpadding="6">
		    <thead>
		        <tr>
		            <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">S.No</th>
	                <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Image</th>
	                <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Category Name</th>
	                <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Item Name</th>
	                <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Amount</th>
		        </tr>
		    </thead>
	    	<tbody>';
	    if ($packageId==0) {
			$Qry="SELECT orders.order_id, orderItem.order_id, orderItem.category_id, orderItem.item_id, category.category_name,cateitems.item_name, cateitems.item_type,cateitems.image,cateitems.price,orders.start_date,orders.end_date FROM tbl_orders AS orders INNER JOIN tbl_order_items AS orderItem ON orderItem.order_id=orders.order_id INNER JOIN tbl_category AS category ON category.category_id=orderItem.category_id INNER JOIN tbl_category_items AS cateitems ON cateitems.item_id=orderItem.item_id where orders.order_id=:order_id";
			$qryParams[":order_id"]=$orderId;
		    $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
		    $i=1;
		    $category_name="";
		    if (count($getResCnt,COUNT_RECURSIVE)>1) {
		    	$OrderDate="";
		        foreach($getResCnt as $getOrderData) {
		            $category_name=(!empty($getOrderData["category_name"]))?$getOrderData["category_name"]:"-";
		            $item_name=(!empty($getOrderData["item_name"]))?$getOrderData["item_name"]:"-";
		            $item_image=(!empty($getOrderData["image"]))?"../".$getOrderData["image"]:"../uploads/category_items/no_food.png";
		            $item_price=(!empty($getOrderData["price"]))?$getOrderData["price"]:"-";
		            $ArrayPrice[]=$item_price;
		            $totalAmt=array_sum($ArrayPrice);
		            if ($getOrderData["start_date"]==$getOrderData["end_date"]) 
	                	$OrderDate = date("d/m/Y",strtotime($getOrderData["start_date"]));
	                else
	                	$OrderDate = date("d/m/Y",strtotime($getOrderData["start_date"]))." - ".date("d/m/Y",strtotime($getOrderData["end_date"]));

			    	$invoiceHTML .= '<tr>'; 
			    	$invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.$i.'</td>';
			        $invoiceHTML .= '<td width="20%" style="font-size:13px;"><img width="60px" height="40px" src="'.$item_image.'" /></td>';
			        $invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.$category_name.'</td>';
			        $invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.$item_name.'</td>';
			        $invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.number_format($item_price,2).'</td>';
			        $invoiceHTML .= '</tr>';
			        $i++;
			    }  
			    $invoiceHTML .= '<tr>';
			    $invoiceHTML .= '<td colspan="4" style="font-size:13px;" valign="middle" align="right"><b>Total Amount</b></td>';
		        $invoiceHTML .= '<td style="font-size:13px;" valign="middle" align="center"><b> $'.number_format($totalAmt,2).'</b></td>';
		        $invoiceHTML .= '</tr>';
		    } else {
		        $invoiceHTML .= '<tr><td colspan="5">No item order(s) found</td></tr>';
		    }
		} else {
			$Qry="SELECT orders.order_id,orders.package_id, packages.package_name,packages.rate,packageitems.item_id,cateitems.item_name,cateitems.image,cat.category_name,orders.start_date,orders.end_date FROM tbl_orders AS orders  INNER JOIN tbl_packages AS packages ON packages.package_id=orders.package_id INNER JOIN tbl_package_items AS packageitems ON packageitems.package_id=packages.package_id INNER JOIN tbl_category_items AS cateitems ON cateitems.item_id=packageitems.item_id left join tbl_category as cat on cat.category_id=cateitems.category_id  where orders.order_id=:orderid";
	        $qryParam[":orderid"]=$orderId;
	        $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParam);
	        $i=1;
	        $package_name="";
	        if (count($getResCnt,COUNT_RECURSIVE)>1) {
	        	$OrderDate="";
	            foreach($getResCnt as $getOrderData) {
	                $category_name=(!empty($getOrderData["category_name"]))?$getOrderData["category_name"]:"-";
	                $package_name=(!empty($getOrderData["package_name"]))?trim($getOrderData["package_name"]):"";
	                $item_name=(!empty($getOrderData["item_name"]))?$getOrderData["item_name"]:"-";
	                $item_image=(!empty($getOrderData["image"]))?"../".$getOrderData["image"]:"../uploads/category_items/no_food.png";
	                $item_price="-";
	                $totalAmt=(!empty($getOrderData["rate"]))?$getOrderData["rate"]:"-";
	                $packageName = $package_name;

	                if ($getOrderData["start_date"]==$getOrderData["end_date"]) 
	                	$OrderDate = date("d/m/Y",strtotime($getOrderData["start_date"]));
	                else
	                	$OrderDate = date("d/m/Y",strtotime($getOrderData["start_date"]))." - ".date("d/m/Y",strtotime($getOrderData["end_date"]));

	            	$invoiceHTML .= '<tr>';
	            	$invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.$i.'</td>';
	                $invoiceHTML .= '<td width="20%" style="font-size:13px;"><img width="60px" height="40px" src="'.$item_image.'" /></td>';
	                $invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.$category_name.'</td>';
	                $invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.$item_name.'</td>';
	                $invoiceHTML .= '<td width="20%" style="font-size:13px;" valign="middle" align="center">'.$item_price.'</td>';
	        		$invoiceHTML .= '</tr>';
	        		$i++;
	         	}
	          	$invoiceHTML .= '<tr>';
			    $invoiceHTML .= '<td colspan="4" style="font-size:13px;" valign="middle" align="right"><b>Total Amount</b></td>';
		        $invoiceHTML .= '<td style="font-size:13px;" valign="middle" align="center"><b> $'.number_format($totalAmt,2).'</b></td>';
		        $invoiceHTML .= '</tr>';
	        } else {
	            $invoiceHTML .= '<tr><td colspan="5">No item order(s) found</td></tr>';
	        }
		}
   	$invoiceHTMLEnd='</tbody>
   	</table>';
   	// echo $invoiceHTML;
 	$htmlpdf=$invoiceHTMLStart.$invoiceHTML.$invoiceHTMLEnd;
	include("create_order_pdf.php");
}
?>