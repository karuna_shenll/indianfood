$(document).ready(function(){

    var kazaFood = new FoodApp();

    var vendorDetArr = {};
    
    vendorDetArr["vendor_id"] = $("#vendors").val();
    vendorDetArr["order_type"] = $("#food_type").val();
    vendorDetArr["HdnPage"] = $("#HdnPage").val();
    vendorDetArr["HdnMode"] = $("#HdnMode").val();
    vendorDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var vendorDetJsonString = JSON.stringify(vendorDetArr);    
    kazaFood.vendorOrderList(vendorDetJsonString);

    $(document).on("click", "#searchbtn", function() {
        // var vendorDetArr = {};
        vendorDetArr["vendor_id"] = $("#vendors").val();
        vendorDetArr["order_type"] = $("#food_type").val();
        vendorDetArr["HdnPage"] = $("#HdnPage").val();
        vendorDetArr["HdnMode"] = $("#HdnMode").val();
        vendorDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var vendorDetJsonString = JSON.stringify(vendorDetArr);
        kazaFood.vendorOrderList(vendorDetJsonString);
    });

    $(document).on("click", "#resetbtn", function() {

        $("#vendors").val("");
        $("#food_type").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        vendorDetArr["vendor_id"] = $("#vendors").val();
        vendorDetArr["order_type"] = $("#food_type").val();
        vendorDetArr["HdnPage"] = $("#HdnPage").val();
        vendorDetArr["HdnMode"] = $("#HdnMode").val();
        vendorDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var vendorDetJsonString = JSON.stringify(vendorDetArr);
        kazaFood.vendorOrderList(vendorDetJsonString);
    });

});