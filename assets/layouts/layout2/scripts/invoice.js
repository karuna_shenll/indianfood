$(document).ready(function(){

    var kazaFood = new FoodApp();
    
    var customerDetArr = {};
    customerDetArr["status"] = $("#status").val();
    customerDetArr["HdnPage"] = $("#HdnPage").val();
    customerDetArr["HdnMode"] = $("#HdnMode").val();
    customerDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var customerDetJsonString = JSON.stringify(customerDetArr);   
    // alert(customerDetJsonString); 
    kazaFood.invoiceList(customerDetJsonString);

    // $(document).on("click","#updateinvoicestatus",function() {
    //     var invoiceID = $(this).attr("data-invoice-id");
    //     $("#payment_status").val($(this).text());
    //     $('#hnd_invoice_id').val(invoiceID);
    //     $("#AddPaymentDetails").modal('show'); 
        
    // });

    $(document).on("change","#payment_status",function() {

        if ($(this).val() == "") {
            $('#payment_status_err').show();
        } else{
            $('#payment_status_err').hide();
        }
    });

    $(document).on("click","#SavePaymentDetails",function() {
        var payment_status = $("#payment_status").val();
        var invoice_id = $("#hnd_invoice_id").val();
        //alert(invoice_id);
        if (payment_status!="" && payment_status!="") {
            $.ajax({
                type: "POST",
                url: "ajax_update_payment_status.php",
                async: false,
                data: {'payment_status':payment_status,'invoice_id':invoice_id},
                // dataType: "json",      
            })
            .success(function(msg) {
                $("#paymentStatusNotificationMsg").show();
                // $("#AddPaymentDetails").modal('show');

                var kazaFood = new FoodApp();
    
                var customerDetArr = {};
                customerDetArr["status"] = $("#status").val();
                customerDetArr["HdnPage"] = $("#HdnPage").val();
                customerDetArr["HdnMode"] = $("#HdnMode").val();
                customerDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
                var customerDetJsonString = JSON.stringify(customerDetArr);   
                kazaFood.invoiceList(customerDetJsonString);
            });
        } else {
            $('#payment_status_err').show();
            return false;
        }
    });
});