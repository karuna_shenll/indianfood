$(document).ready(function(){

    var kazaFood = new FoodApp();
    
    var invoiceDetArr = {};
    invoiceDetArr["vendorsid"] = $("#vendors").val();
    invoiceDetArr["HdnPage"] = $("#HdnPage").val();
    invoiceDetArr["HdnMode"] = $("#HdnMode").val();
    invoiceDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var invoiceDetJsonString = JSON.stringify(invoiceDetArr);    
    kazaFood.invoiceOrderList(invoiceDetJsonString);

    $(document).on("click", "#Search", function() {

        if ($("#vendors").val() != "") {
            invoiceDetArr["vendorsid"] = $("#vendors").val();
            invoiceDetArr["HdnPage"] = $("#HdnPage").val();
            invoiceDetArr["HdnMode"] = $("#HdnMode").val();
            invoiceDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
            var invoiceDetJsonString = JSON.stringify(invoiceDetArr);
            kazaFood.invoiceOrderList(invoiceDetJsonString);
        }
    });

    $(document).on("click", "#invoiceReset", function() {

        $("#vendors").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        invoiceDetArr["vendorsid"] = $("#vendors").val();
        invoiceDetArr["HdnPage"] = $("#HdnPage").val();
        invoiceDetArr["HdnMode"] = $("#HdnMode").val();
        invoiceDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var invoiceDetJsonString = JSON.stringify(invoiceDetArr);
        kazaFood.invoiceOrderList(invoiceDetJsonString);
    });

    
});