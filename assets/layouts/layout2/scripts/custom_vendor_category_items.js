 $(document).ready(function(){

    var kazaFood = new FoodApp();

    var vendorDetArr = {};
 	vendorDetArr["vendor_id"]       = $("#HdnVendor").val();
    vendorDetArr["item_type"]       = $("#item_type").val();
    vendorDetArr["item_name"]       = $("#item_name").val();
    // vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
    // vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
    // vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();
    var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
    kazaFood.vendorCategoryItemList(vendorDetJsonString);
    
    $(document).on("click", "#searchbtn", function() {
        // var vendorDetArr = {};        
        vendorDetArr["vendor_id"]       = $("#HdnVendor").val();
	    vendorDetArr["item_type"]       = $("#item_type").val();
	    vendorDetArr["item_name"]       = $("#item_name").val();
	    // vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
	    // vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
	    // vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();	    
        var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
        kazaFood.vendorCategoryItemList(vendorDetJsonString);
    });

    $(document).on("click", "#resetbtn", function() {        
        $("#item_type").val("");
        $("#item_name").val("");
        // $("#HdnPage").val("1");
        // $("#HdnMode").val("1");
        // $("#RecordsPerPage").val("25");

        vendorDetArr["vendor_id"] 		= $("#HdnVendor").val();
        vendorDetArr["item_type"] 	    = $("#item_type").val();
        vendorDetArr["item_name"] 	    = $("#item_name").val();
        // vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
        // vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
        // vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();
        var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
        kazaFood.vendorCategoryItemList(vendorDetJsonString);
    });          
});