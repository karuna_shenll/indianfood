<?php
include('common.php');
class indianfoodapp extends common {
	// public $dbconn;
	// public function __construct(PDO $dbconn){
	// 	$this->dbconn = $dbconn;
	// }
	public function funLoginDetails($email,$password,$usertype) {
		if (!empty($email) && !empty($password) && !empty($usertype)) { 
			$ArrLoginDetails["status"]="0";
			$ArrLoginDetails["message"]="success";
            $ArrLoginDetails["userId"]="121";
			return $this->funjsonReturn($ArrLoginDetails);
	    } else {
			$ArrLoginDetails["status"]="1";
            $ArrLoginDetails["message"]="failure";
            return $this->funjsonReturn($ArrLoginDetails);
	    }
	}
	public function funRegisterDetails($name,$mobile,$address,$country,$zip,$email,$password,$usertype,$deviceToken,$deviceInfo) {

		if (!empty($name) && !empty($mobile) && !empty($address) && !empty($country) && !empty($zip) && !empty($email) && !empty($password) && !empty($usertype) && !empty($deviceToken) && !empty($deviceInfo) ) { 
			$ArrRegisterDetails["status"]="0";
			$ArrRegisterDetails["message"]="success";
            $ArrRegisterDetails["userId"]="122";
			return $this->funjsonReturn($ArrRegisterDetails);
	    } else {
			$ArrRegisterDetails["status"]="1";
            $ArrRegisterDetails["message"]="failure";
            return $this->funjsonReturn($ArrRegisterDetails);
	    }
	}

	public function funForgetPasswordDetails($email,$usertype) {
        if (!empty($email) && !empty($usertype) ) { 
        	$ArrForgetPasswordDetails["status"]="0";
			$ArrForgetPasswordDetails["message"]="success";
			return $this->funjsonReturn($ArrForgetPasswordDetails);
		} else {
            $ArrForgetPasswordDetails["status"]="1";
            $ArrForgetPasswordDetails["message"]="failure";
            return $this->funjsonReturn($ArrForgetPasswordDetails);
		}

	}

	public function funVendorListDetails($gettype){
		if (!empty($gettype)) { 
        	$ArrVendorListDetails["status"]="0";
			$ArrVendorListDetails["message"]="success";
			$ArrVendorListDetails["vendor"][]=array(
				"vendorId"=>"1",
				"vendorname"=>"dfa",
				"details"=>"were",
				"ratings"=>"5",
					"mainDish"=>array( 
						array (
		                "dishId"=>"123",
		                "dishName"=>"rice",
		                "price"=> "10",
		                "image"=>"",
		                "type"=>"veg"
		                 ),
						array (
		                "dishId"=>"123",
		                "dishName"=>"briyani",
		                "price"=> "10",
		                "image"=>"",
		                "type"=>"nonveg"
		                ),
                	),
                	"sideDish"=>array( 
                        array (
			                "dishId"=>"123",
			                "dishName"=>"rice",
			                "price"=>"10",
			                "image"=>"",
			                "type"=>"veg"
				        ),
				        array (
			                "dishId"=>"123",
			                "dishName"=>"rice",
			                "price"=>"10",
			                "image"=>"",
			                "type"=>"Halal"
				        ),
       				),
					"packages"=>array(
						array (
							"packageId"=>"123",
			                "packageName"=>"rice",
			                "price"=> "10",
			                "maindishCount"=>"2",
			                "maindishList"=>"egg, tea",
			                "sidedishCount"=>"2",
			                "sidedishList"=>"egg, tea",
			                "image"=>"",
			                "type"=>"veg"
						),
						array (
							"packageId"=>"123",
			                "packageName"=>"rice",
			                "price"=> "10",
			                "maindishCount"=>"3",
			                "maindishList"=>"egg, tea",
			                "sidedishCount"=>"3",
			                "sidedishList"=>"egg, tea",
			                "image"=>"",
			                "type"=>"Halal",
						),
					),
        		);
			
			return $this->funjsonReturn($ArrVendorListDetails);
		} else {
            $ArrVendorListDetails["status"]="1";
            $ArrVendorListDetails["message"]="failure";
            return $this->funjsonReturn($ArrVendorListDetails);
		}
	}

	public function funMyOrderDetails($userid) {

		if (!empty($userid)) { 

			$ArrMyOrderDetails["status"]="0";
			$ArrMyOrderDetails["message"]="success";
			$ArrMyOrderDetails["orders"]=array( 
				array (
                 	"orderId"=> "1",
				    "vendorname"=> "dfa",
				    "ratings"=> "5",
				    "status"=>"pending",
                 ),
				array (
	                "orderId"=> "2",
					"vendorname"=> "dfa",
					"ratings"=> "3",
					"status"=>"pending",
                ),
            );
          return $this->funjsonReturn($ArrMyOrderDetails);
		} else {

			$ArrMyOrderDetails["status"]="1";
            $ArrMyOrderDetails["message"]="failure";
            return $this->funjsonReturn($ArrMyOrderDetails);
        }

	}

	public function funOrderHistoryDetails($userid) {

		if (!empty($userid)) { 

			$ArrOrderHistoryDetails["status"]="0";
			$ArrOrderHistoryDetails["message"]="success";
			$ArrOrderHistoryDetails["orders"]=array( 
				array (
             	   "orderId"=>"1",
				   "vendorname"=>"dfa",
				   "ratings"=>"5",
				   "status"=>"confirm",
				   "orderDate"=>"2017/10/05",
				   "orderTime"=>"12:00",
				   "ordersList"=>array( 
						array (
		               		"name"=>"egg",
            				"type"=>"veg",
		                 ),
						array (
		                    "name"=>"egg",
            				"type"=>"veg",
		                ),
                	),
                 ),
				array (
             	   "orderId"=>"2",
				   "vendorname"=>"zxcvzvzxc",
				   "ratings"=>"3",
				   "status"=>"pending",
				   "orderDate"=>"2017/10/05",
				   "orderTime"=>"13:00",
				   "ordersList"=>array( 
						array (
		               		"name"=>"egg",
            				"type"=>"veg",
		                 ),
						array (
		                    "name"=>"egg",
            				"type"=>"veg",
		                ),
                	),
                 ),
            );
          return $this->funjsonReturn($ArrOrderHistoryDetails);
		} else {

			$ArrOrderHistoryDetails["status"]="1";
            $ArrOrderHistoryDetails["message"]="failure";
            return $this->funjsonReturn($ArrOrderHistoryDetails);
        }

	}

	public function funUpdateOrderDetails($userid,$orderid,$orderstatus) {
		if (!empty($userid) && !empty($orderid) && !empty($orderstatus)) { 
			$ArrUpdateOrderDetails["status"]="0";
            $ArrUpdateOrderDetails["message"]="success";
            $ArrUpdateOrderDetails["orderStatus"]=$orderstatus;
            return $this->funjsonReturn($ArrUpdateOrderDetails);
		} else {
			$ArrUpdateOrderDetails["status"]="1";
            $ArrUpdateOrderDetails["message"]="failure";
            return $this->funjsonReturn($ArrUpdateOrderDetails);
		}

	}
	public function funCreateUpdateCategoryDetails($userid,$categoryid,$categoryorder,$vendorid,$categoryname,$description,$image)
    {
        if (!empty($categoryid) &&  !empty($userid)) {

        	$ArrCreateUpdateCategoryDetails["status"]="0";
            $ArrCreateUpdateCategoryDetails["message"]="success";
            return $this->funjsonReturn($ArrCreateUpdateCategoryDetails);
        }
        else if(empty($categoryid) && !empty($userid)) {
        	$ArrCreateUpdateCategoryDetails["status"]="0";
            $ArrCreateUpdateCategoryDetails["message"]="success";
            return $this->funjsonReturn($ArrCreateUpdateCategoryDetails);

        } else {
            $ArrCreateUpdateCategoryDetails["status"]="1";
            $ArrCreateUpdateCategoryDetails["message"]="failure";
            return $this->funjsonReturn($ArrCreateUpdateCategoryDetails);
        }
	}

	public function funDeleteCategoryDetails($userid,$categoryid) {

		 if (!empty($categoryid) && !empty($userid)) {

        	$ArrDeleteCategoryDetails["status"]="0";
            $ArrDeleteCategoryDetails["message"]="success";
            return $this->funjsonReturn($ArrDeleteCategoryDetails);
        }
        else {
            $ArrDeleteCategoryDetails["status"]="1";
            $ArrDeleteCategoryDetails["message"]="failure";
            return $this->funjsonReturn($ArrDeleteCategoryDetails);
        }

	}
    public function funCreateUpdateItemDetails($userid,$categoryid,$itemid,$itemname,$description,$price,$itemtype,$image)
    {
        if (!empty($itemid)) {

        	$ArrCreateUpdateItemDetails["status"]="0";
            $ArrCreateUpdateItemDetails["message"]="success";
            return $this->funjsonReturn($ArrCreateUpdateItemDetails);
        }
        else if(empty($itemid)) {
        	$ArrCreateUpdateItemDetails["status"]="0";
            $ArrCreateUpdateItemDetails["message"]="success";
            return $this->funjsonReturn($ArrCreateUpdateItemDetails);

        } else {
            $ArrCreateUpdateItemDetails["status"]="1";
            $ArrCreateUpdateItemDetails["message"]="failure";
            return $this->funjsonReturn($ArrCreateUpdateItemDetails);
        }
	}
	public function funDeleteItemDetails($userid,$itemid) {

		if (!empty($itemid) && !empty($userid)) {
			$ArrDeleteItemDetails["status"]="0";
            $ArrDeleteItemDetails["message"]="success";
            return $this->funjsonReturn($ArrDeleteItemDetails);
        }
        else {
            $ArrDeleteItemDetails["status"]="1";
            $ArrDeleteItemDetails["message"]="failure";
            return $this->funjsonReturn($ArrDeleteItemDetails);
        }

	}

	public function funUpdateOrderStatusDetails($userid,$orderId,$orderStatus,$reason) {
         if (!empty($orderId) && !empty($userid)) {
			$ArrUpdateOrderStatusDetails["status"]="0";
            $ArrUpdateOrderStatusDetails["message"]="success";
            $ArrUpdateOrderStatusDetails["orderStatus"]=$orderStatus;
            return $this->funjsonReturn($ArrUpdateOrderStatusDetails);
        }
        else {
            $ArrUpdateOrderStatusDetails["status"]="1";
            $ArrUpdateOrderStatusDetails["message"]="failure";
            return $this->funjsonReturn($ArrUpdateOrderStatusDetails);
        }
	}

	public function funCustomerDetails($userid) {
		 if (!empty($userid)) {
		 	$ArrCustomerDetails["status"]="0";
			$ArrCustomerDetails["message"]="success";
			$ArrCustomerDetails["customers"]=array( 
				array (
             	   "cusId"=>"123",
				   "cusName "=>"John",
				   "cusEmail"=>"test@gmail.com",
				   "cusPhone"=>"4365363456544",
				   "rate"=>"45",
				   "ordersList"=>array( 
						array (
		               		"name"=>"egg",
            				"type"=>"veg",
		                 ),
						array (
		                    "name"=>"egg",
            				"type"=>"veg",
		                ),
                	),
                 ),
            );
          return $this->funjsonReturn($ArrCustomerDetails);
		 } else {
            $ArrCustomerDetails["status"]="1";
            $ArrCustomerDetails["message"]="failure";
            return $this->funjsonReturn($ArrCustomerDetails);
		 }
	}

	public function funGetReport($userid,$startdate,$enddate){
		if (!empty($userid) && !empty($startdate) && !empty($enddate)) {

			$ArrGetReport["status"]="0";
            $ArrGetReport["message"]="success";
            
            $ArrGetReport["customers"]=array(
            	$arrayName = array(
            		"cusId"=>"123",
					"cusName"=>"john",
					"price"=>"1232" ),
            );
            $ArrGetReport["total"]="2234";
           return $this->funjsonReturn($ArrGetReport);
		} else {
            $ArrGetReport["status"]="1";
            $ArrGetReport["message"]="failure";
            return $this->funjsonReturn($ArrGetReport);
		}
	} 

	public function funGetInvoice($userid) {

		if (!empty($userid)) {

			$ArrGetInvoice["status"]="0";
            $ArrGetInvoice["message"]="success";
            
            $ArrGetInvoice["invoice"]=array(
            	$arrayName = array(
            		"invoiceId"=>"123",
					"status "=>"Pending",
					"price"=>"1232" ),
            );
           
           return $this->funjsonReturn($ArrGetInvoice);
		} else {
            $ArrGetInvoice["status"]="1";
            $ArrGetInvoice["message"]="failure";
            return $this->funjsonReturn($ArrGetInvoice);
		}

	}
	public function funUpdateInvoice($userid,$invoiceId,$invoiceStatus) {

		if (!empty($userid) && !empty($invoiceId)) {

			$ArrUpdateInvoice["status"]="0";
            $ArrUpdateInvoice["message"]="success";
            $ArrUpdateInvoice["invoiceStatus"]=$invoiceStatus;
            return $this->funjsonReturn($ArrUpdateInvoice);
		} else {
            $ArrUpdateInvoice["status"]="1";
            $ArrUpdateInvoice["message"]="failure";
            return $this->funjsonReturn($ArrUpdateInvoice);
		}

	}

	public function funUpdateRating($userid,$orderId,$vendorId,$rate) {

		if (!empty($userid) && !empty($orderId) && !empty($vendorId) && !empty($rate)) {

			$ArrUpdateRating["status"]="0";
            $ArrUpdateRating["message"]="success";
            return $this->funjsonReturn($ArrUpdateRating);
		} else {
            $ArrUpdateRating["status"]="1";
            $ArrUpdateRating["message"]="failure";
            return $this->funjsonReturn($ArrUpdateRating);
		}


	}

	public function funFoodTypeDetails($gettype){
		if(!empty($gettype)){
            $ArrFoodTypeDetails["status"]="0";
            $ArrFoodTypeDetails["message"]="success";
            $ArrFoodTypeDetails["foodType"]=array(
            	$arrayName = array(
            		"typeId"=>"1",
					"typeName "=>"Veg"
				),
            );
            return $this->funjsonReturn($ArrFoodTypeDetails);
		} else {
			$ArrFoodTypeDetails["status"]="1";
            $ArrFoodTypeDetails["message"]="failure";
            return $this->funjsonReturn($ArrFoodTypeDetails);
		}
	}

	public function funCategoryListDetails($gettype){
		// return "test";
		if(!empty($gettype)){
            $ArrCategoryListDetails["status"]="0";
            $ArrCategoryListDetails["message"]="success";
            $ArrCategoryListDetails["foodType"]=array(
            	$arrayName = array(
            		"categoryId"=>"1",
					"categoryName "=>"rice"
				),
            );
            return $this->funjsonReturn($ArrCategoryListDetails);
		} else {
			$ArrCategoryListDetails["status"]="1";
            $ArrCategoryListDetails["message"]="failure";
            return $this->funjsonReturn($ArrCategoryListDetails);
		}
	}
	public function funConfirmOrder($gettype,$confirmJson) {
		if(!empty($gettype) && !empty($confirmJson)){

            $ArrConfirmOrderDetails["status"]="0";
            $ArrConfirmOrderDetails["message"]="success";
            $ArrConfirmOrderDetails["orderId"]="123213";
            return $this->funjsonReturn($ArrConfirmOrderDetails);
        } else {
            $ArrConfirmOrderDetails["status"]="1";
            $ArrConfirmOrderDetails["message"]="failure";
            return $this->funjsonReturn($ArrConfirmOrderDetails);
        }
	}
	public function funAddPackageEdit($gettype,$confirmJson) {
		if(!empty($gettype) && !empty($confirmJson)){

            $ArrAddPackageEditDetails["status"]="0";
            $ArrAddPackageEditDetails["message"]="success";
            return $this->funjsonReturn($ArrAddPackageEditDetails);
        } else {
            $ArrAddPackageEditDetails["status"]="1";
            $ArrAddPackageEditDetails["message"]="failure";
            return $this->funjsonReturn($ArrAddPackageEditDetails);
        }
	}
	public function funUpdateProfileDetails($userid,$name,$mobile,$address,$country,$zip,$email,$password,$type,$documentupload,$deviceToken,$deviceInfo){

		if(!empty($userid)){
            $ArrUpdateProfileDetails["status"]="0";
            $ArrUpdateProfileDetails["message"]="success";
            return $this->funjsonReturn($ArrUpdateProfileDetails);
		} else {
           $ArrUpdateProfileDetails["status"]="1";
           $ArrUpdateProfileDetails["message"]="failure";
           return $this->funjsonReturn($ArrUpdateProfileDetails);
		}

	}
}
?>